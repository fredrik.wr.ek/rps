package com.game.rps.gameuser;

import com.game.rps.game.GameEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface GameUserJoinerRepository extends JpaRepository<GameUserJoinerEntity, String> {
    GameUserJoinerEntity findByGameId(GameEntity gameEntity);
}
