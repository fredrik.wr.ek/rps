package com.game.rps.gameuser;

import com.game.rps.enums.Game;
import com.game.rps.game.GameEntity;
import com.game.rps.enums.Move;
import com.game.rps.user.UserEntity;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity(name= "gamejoiner")
@Data
@AllArgsConstructor
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class GameUserJoinerEntity {
    @Id
    String id;
    Move move;
    Game game;

    @ManyToOne
    @JoinColumn(name = "game_id", nullable = false)
    GameEntity gameId;

    @ManyToOne
    @JoinColumn(name = "user_id", nullable = false)
    UserEntity userId;
}
