package com.game.rps.gameuser;


import com.game.rps.enums.Game;
import com.game.rps.game.GameEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface GameUserOwnerRepository extends JpaRepository<GameUserOwnerEntity, String> {
    GameUserOwnerEntity findByGameId(GameEntity gameEntity);
    List<GameUserOwnerEntity> findAllByGame(Game game);
}
