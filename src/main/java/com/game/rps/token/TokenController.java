package com.game.rps.token;

import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/auth")
@AllArgsConstructor
public class TokenController {
    TokenService tokenService;

    @GetMapping("/token")
    public String getToken() { return tokenService.createToken(); }
}
