package com.game.rps.token;

import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

@Service
public class TokenService {
    private final Map<String, Token> tokens = new HashMap<>();

    public String createToken() {
        Token token = Token.create();
        tokens.put(token.getId(), token);
        return token.getId();
    }
    public  Token getTokenById(String tokenId) {
        return tokens.get(tokenId);
    }
    public void updateToken(Token token) {
        tokens.put(token.getId(), token);
    }
}
