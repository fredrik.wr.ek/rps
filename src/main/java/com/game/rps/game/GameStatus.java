package com.game.rps.game;

import com.game.rps.enums.Game;
import com.game.rps.enums.Move;
import lombok.Builder;
import lombok.Value;

@Value
@Builder(toBuilder = true)
public class GameStatus {
    String gameId;
    String name;
    Move move;
    Game game;
    String opponentName;
    Move opponentMove;
}
