package com.game.rps.game;

import com.game.rps.enums.Game;
import com.game.rps.enums.Move;
import com.game.rps.gameuser.GameUserJoinerEntity;
import com.game.rps.gameuser.GameUserJoinerRepository;
import com.game.rps.gameuser.GameUserOwnerEntity;
import com.game.rps.gameuser.GameUserOwnerRepository;
import com.game.rps.user.UserEntity;
import com.game.rps.user.UserNotFoundException;
import com.game.rps.user.UserRepository;
import org.springframework.stereotype.Service;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;

@Service
public class GameService {

    private final GameRepository gameRepository;
    private final GameUserOwnerRepository gameUserOwnerRepository;
    private final GameUserJoinerRepository gameUserJoinerRepository;
    private final UserRepository userRepository;

    public GameService(GameRepository gameRepository,
                       GameUserOwnerRepository gameUserOwnerRepository,
                       GameUserJoinerRepository gameUserJoinerRepository,
                       UserRepository userRepository
    ) {
        this.gameRepository = gameRepository;
        this.gameUserOwnerRepository = gameUserOwnerRepository;
        this.gameUserJoinerRepository = gameUserJoinerRepository;
        this.userRepository = userRepository;
    }

    public GameStatus updateMove(String sign, UserEntity userEntity) throws UserNotFoundException, GameNotFoundException {
        GameStatus gameStatus = getGameStatus(userEntity);
        GameEntity gameEntity = gameRepository.getById(gameStatus.getGameId());
        GameUserOwnerEntity gameUserOwnerEntity = gameUserOwnerRepository.findByGameId(gameEntity);
        GameUserJoinerEntity gameUserJoinerEntity = gameUserJoinerRepository.findByGameId(gameEntity);
        return createGameStatus(
                gameStatus.getGameId(),
                gameStatus.getName(),
                saveMove(sign, userEntity, gameUserOwnerEntity, gameUserJoinerEntity),
                getResult(userEntity, gameEntity, gameUserOwnerEntity, gameUserJoinerEntity),
                gameStatus.getOpponentName(),
                gameStatus.getOpponentMove());
    }

    public Game getResult(UserEntity userEntity, GameEntity gameEntity, GameUserOwnerEntity gameUserOwnerEntity, GameUserJoinerEntity gameUserJoinerEntity) throws UserNotFoundException {
        if(gameUserOwnerEntity.getUserId().equals(userEntity))
            return saveResultIfOwner(userEntity, gameEntity, gameUserOwnerEntity, gameUserJoinerEntity);
        if(gameUserJoinerEntity.getUserId().equals(userEntity))
            return saveResultIfJoiner(userEntity, gameEntity, gameUserJoinerEntity, gameUserOwnerEntity);
        return Game.ACTIVE;
    }

    public Game saveResultIfOwner(UserEntity userEntity, GameEntity gameEntity, GameUserOwnerEntity gameUserOwnerEntity, GameUserJoinerEntity gameUserJoinerEntity) throws UserNotFoundException {
        UserEntity opponent = getGameJoiner(gameUserJoinerEntity);
        Game result = checkWinner(gameUserJoinerEntity.getMove(), gameUserOwnerEntity.getMove());
        updateGameUserJoinerEntity(gameUserJoinerEntity.getId(), gameUserJoinerEntity.getMove(), result, gameEntity, opponent);
        result = checkWinner(gameUserOwnerEntity.getMove(), gameUserJoinerEntity.getMove());
        updateGameUserOwnerEntity(gameUserOwnerEntity.getId(), gameUserOwnerEntity.getMove(), result, gameEntity, userEntity);
        return result;
    }

    public Game saveResultIfJoiner(UserEntity userEntity, GameEntity gameEntity, GameUserJoinerEntity gameUserJoinerEntity, GameUserOwnerEntity gameUserOwnerEntity) throws UserNotFoundException {
        UserEntity opponent = getGameOwner(gameUserOwnerEntity);
        Game result = checkWinner(gameUserOwnerEntity.getMove(), gameUserJoinerEntity.getMove());
        updateGameUserOwnerEntity(gameUserOwnerEntity.getId(), gameUserOwnerEntity.getMove(), result, gameEntity, opponent);
        result = checkWinner(gameUserJoinerEntity.getMove(), gameUserOwnerEntity.getMove());
        updateGameUserJoinerEntity(gameUserJoinerEntity.getId(), gameUserJoinerEntity.getMove(), result, gameEntity, userEntity);
        return result;
    }

    public Game checkWinner(Move move, Move opponentMove) {
        if(move == opponentMove)
            return Game.DRAW;
        if(opponentMove == Move.NONE)
            return Game.ACTIVE;
        if(move == Move.PAPER && opponentMove == Move.ROCK)
            return Game.WIN;
        if(move == Move.ROCK && opponentMove == Move.SCISSOR)
            return Game.WIN;
        if(move == Move.SCISSOR && opponentMove == Move.PAPER)
            return Game.WIN;
        return Game.LOSE;
    }

    public Move saveMove(String sign, UserEntity userEntity, GameUserOwnerEntity gameUserOwnerEntity, GameUserJoinerEntity gameUserJoinerEntity) {
        Move move = formatSign(sign);
        saveGameOwnerMove(move, userEntity, gameUserOwnerEntity);
        saveGameJoinerMove(move, userEntity, gameUserJoinerEntity);
        return move;
    }

    public void saveGameOwnerMove(Move move, UserEntity userEntity, GameUserOwnerEntity gameUserOwnerEntity) {
        if(gameUserOwnerEntity.getUserId().equals(userEntity)) {
            gameUserOwnerEntity.setMove(move);
            gameUserOwnerRepository.save(gameUserOwnerEntity);
        }
    }

    public void saveGameJoinerMove(Move move, UserEntity userEntity, GameUserJoinerEntity gameUserJoinerEntity) {
        if(gameUserJoinerEntity.getUserId().equals(userEntity)) {
            gameUserJoinerEntity.setMove(move);
            gameUserJoinerRepository.save(gameUserJoinerEntity);
        }
    }

    public Move formatSign(String sign) {
        return Move.valueOf(sign.toUpperCase());
    }

    public GameStatus getGameInfo(String gameId) throws UserNotFoundException {
        GameEntity gameEntity = gameRepository.getById(gameId);
        GameUserOwnerEntity gameUserOwnerEntity = gameUserOwnerRepository.findByGameId(gameEntity);
        GameUserJoinerEntity gameUserJoinerEntity = gameUserJoinerRepository.findByGameId(gameEntity);
        UserEntity gameOwner = getGameOwner(gameUserOwnerEntity);
        UserEntity gameJoiner = getGameJoiner(gameUserJoinerEntity);
        return createGameStatus(
                gameId,
                gameOwner.getName(),
                gameUserOwnerEntity.getMove(),
                gameUserOwnerEntity.getGame(),
                gameJoiner != null ? gameJoiner.getName() : null,
                gameJoiner != null ? gameUserJoinerEntity.getMove() : Move.NONE
        );
    }

    public GameStatus createGame(UserEntity userEntity) throws ActiveGameAlreadyExistException {
        checkIfUserInActiveGame(userEntity);
        GameEntity gameEntity = addGameToDataBase();
        GameUserOwnerEntity gameUserOwnerEntity = updateGameUserOwnerEntity(UUID.randomUUID().toString(),
                Move.NONE,
                Game.OPEN,
                gameEntity,
                userEntity);
        return createGameStatus(
                gameEntity.getId(),
                userEntity.getName(),
                gameUserOwnerEntity.getMove(),
                gameUserOwnerEntity.getGame(),
                null,
                Move.NONE
        );
    }

    public void checkIfUserInActiveGame(UserEntity userEntity) throws ActiveGameAlreadyExistException {
        boolean checkOwner = gameUserOwnerRepository
                .findAll().stream()
                .anyMatch(findActiveGameOwnerEntityByUserEntity(userEntity));
        boolean checkJoiner = gameUserJoinerRepository
                .findAll().stream()
                .anyMatch(findActiveGameJoinerEntityByUserEntity(userEntity));
        if(checkOwner || checkJoiner) throw new ActiveGameAlreadyExistException();
    }

    public List<GameStatus> getOpenGameList() {
        List<GameUserOwnerEntity> openGames = gameUserOwnerRepository.findAllByGame(Game.OPEN);
        List<UserEntity> gameOwners = userRepository
                .findAll()
                .stream()
                .filter(filterGameOwnersByGames(openGames)).collect(Collectors.toList());
        return mergeToGameStatusList(gameOwners,openGames);
    }

    public List<GameStatus> mergeToGameStatusList(List<UserEntity> userEntities, List<GameUserOwnerEntity> gameUserOwnerEntityList){
        return userEntities.stream().map(owner -> {
            Optional<GameUserOwnerEntity> gameUserOwnerEntity = gameUserOwnerEntityList
                    .stream()
                    .filter(game -> game.getUserId().getId().equals(owner.getId()))
                    .findAny();
            return gameUserOwnerEntity.map(mergeToGameStatus(owner)).orElse(null);
        }).collect(Collectors.toList());
    }

    public Function<GameUserOwnerEntity, GameStatus> mergeToGameStatus(UserEntity userEntity) {
        return gameUserOwnerEntity -> createGameStatus(
                gameUserOwnerEntity.getGameId().getId(),
                userEntity.getName(),
                gameUserOwnerEntity.getMove(),
                gameUserOwnerEntity.getGame(),
                null,
                Move.NONE
        );
    }

    public Predicate<UserEntity> filterGameOwnersByGames(List<GameUserOwnerEntity> gameUserOwnerEntityList) {
        return  gameOwner -> gameUserOwnerEntityList
                .stream()
                .anyMatch(game -> game.getUserId()
                        .getId()
                        .equals(gameOwner.getId()));
    }

    public GameStatus joinGame(String gameId, UserEntity userEntity) throws UserNotFoundException, ActiveGameAlreadyExistException {
        checkIfUserInActiveGame(userEntity);
        GameEntity gameEntity = gameRepository.getById(gameId);
        GameUserJoinerEntity gameUserJoinerEntity = addGameUserJoinerEntity(gameEntity, userEntity);
        UserEntity gameOwner = updateGameOwner(gameEntity, Move.NONE, Game.ACTIVE);
        return createGameStatus(
                gameEntity.getId(),
                userEntity.getName(),
                gameUserJoinerEntity.getMove(),
                gameUserJoinerEntity.getGame(),
                gameOwner.getName(),
                Move.NONE
        );
    }

    public GameStatus createGameStatus(
            String gameId,
            String name,
            Move move,
            Game game,
            String opponentName,
            Move opponentMove)
    {
        return GameStatus.builder()
                .gameId(gameId)
                .name(name)
                .move(move)
                .game(game)
                .opponentName(opponentName)
                .opponentMove(opponentMove)
                .build();
    }

    public GameStatus getGameStatus(UserEntity userEntity) throws UserNotFoundException, GameNotFoundException {
        List<GameUserOwnerEntity> gameUserOwnerEntityList = gameUserOwnerRepository.findAll();
        List<GameUserJoinerEntity> gameUserJoinerEntityList = gameUserJoinerRepository.findAll();
        Optional<GameUserOwnerEntity> gameUserOwnerEntity = findActiveGameOwner(userEntity, gameUserOwnerEntityList);
        Optional<GameUserJoinerEntity> gameUserJoinerEntity = findActiveGameJoiner(userEntity, gameUserJoinerEntityList);
        if(gameUserOwnerEntity.isPresent())
            return getGameStatusIfOwner(userEntity, gameUserOwnerEntity.get(), gameUserJoinerEntityList);
        if(gameUserJoinerEntity.isPresent())
            return getGameStatusIfJoiner(userEntity, gameUserJoinerEntity.get(), gameUserOwnerEntityList);
        throw new GameNotFoundException();
    }

    public Optional<GameUserJoinerEntity> findActiveGameJoiner(UserEntity userEntity, List<GameUserJoinerEntity> gameUserJoinerEntityList) {
        return gameUserJoinerEntityList.stream()
                .filter(findActiveGameJoinerEntityByUserEntity(userEntity))
                .findAny();
    }

    public Optional<GameUserOwnerEntity> findActiveGameOwner(UserEntity userEntity, List<GameUserOwnerEntity> gameUserOwnerEntityList) {
        return gameUserOwnerEntityList.stream()
                .filter(findActiveGameOwnerEntityByUserEntity(userEntity))
                .findAny();
    }

    public GameStatus getGameStatusIfJoiner(
            UserEntity userEntity,
            GameUserJoinerEntity gameUserJoinerEntity,
            List<GameUserOwnerEntity> gameUserOwnerEntityList) throws UserNotFoundException
    {
        Optional<GameUserOwnerEntity> gameUserOwnerEntity = gameUserOwnerEntityList.stream()
                .filter(item -> item.getGameId().getId().equals(gameUserJoinerEntity.getGameId().getId())).findAny();
        return createGameStatus(
                gameUserJoinerEntity.getGameId().getId(),
                userEntity.getName(),
                gameUserJoinerEntity.getMove(),
                gameUserJoinerEntity.getGame(),
                getGameOwnerByGameEntity(gameUserJoinerEntity.getGameId()).getName(),
                gameUserOwnerEntity.isPresent() ? gameUserOwnerEntity.get().getMove() : Move.NONE
        );
    }

    public GameStatus getGameStatusIfOwner(
            UserEntity userEntity,
            GameUserOwnerEntity gameUserOwnerEntity,
            List<GameUserJoinerEntity> gameUserJoinerEntityList) throws UserNotFoundException
    {
            Optional<GameUserJoinerEntity> gameUserJoinerEntity = gameUserJoinerEntityList.stream()
                    .filter(item -> item.getGameId().getId().equals(gameUserOwnerEntity.getGameId().getId()))
                    .findAny();
            return createGameStatus(
                    gameUserOwnerEntity.getGameId().getId(),
                    userEntity.getName(),
                    gameUserOwnerEntity.getMove(),
                    gameUserOwnerEntity.getGame(),
                    gameUserJoinerEntity.isPresent() ? getGameJoinerByGameEntity(gameUserOwnerEntity.getGameId()).getName() : null,
                    gameUserJoinerEntity.isPresent() ? gameUserJoinerEntity.get().getMove() : Move.NONE
            );
    }

    private Predicate<GameUserOwnerEntity> findActiveGameOwnerEntityByUserEntity(UserEntity userEntity) {
        return item ->
                item.getGame() != Game.NONE
                        && item.getUserId()
                                .getId()
                                .equals(userEntity.getId());
    }

    private Predicate<GameUserJoinerEntity> findActiveGameJoinerEntityByUserEntity(UserEntity userEntity) {
        return item ->
                item.getGame() != Game.NONE
                        && item.getUserId()
                        .getId()
                        .equals(userEntity.getId());
    }

    public GameEntity addGameToDataBase() {
        GameEntity gameEntity = new GameEntity(UUID.randomUUID().toString());
        return gameRepository.save(gameEntity);
    }

    public GameUserOwnerEntity updateGameUserOwnerEntity(
            String id,
            Move move,
            Game game,
            GameEntity gameEntity,
            UserEntity userEntity)
    {
        GameUserOwnerEntity gameUserOwnerEntity = new GameUserOwnerEntity(
                id,
                move,
                game,
                gameEntity,
                userEntity
        );
        return gameUserOwnerRepository.save(gameUserOwnerEntity);
    }

    public void updateGameUserJoinerEntity(
            String id,
            Move move,
            Game game,
            GameEntity gameEntity,
            UserEntity userEntity)
    {
        GameUserJoinerEntity gameUserJoinerEntity = new GameUserJoinerEntity(
                id,
                move,
                game,
                gameEntity,
                userEntity
        );
        gameUserJoinerRepository.save(gameUserJoinerEntity);
    }

    public GameUserJoinerEntity addGameUserJoinerEntity(GameEntity gameEntity, UserEntity userEntity) {
        GameUserJoinerEntity gameUserJoinerEntity = new GameUserJoinerEntity(
                UUID.randomUUID().toString(),
                Move.NONE,
                Game.ACTIVE,
                gameEntity,
                userEntity
        );
        return gameUserJoinerRepository.save(gameUserJoinerEntity);
    }

    public UserEntity getGameOwner(GameUserOwnerEntity gameUserOwnerEntity) throws UserNotFoundException {
        if(gameUserOwnerEntity == null)
            return null;
        return userRepository
                .findById(gameUserOwnerEntity.getUserId().getId())
                .orElseThrow(UserNotFoundException::new);
    }

    public UserEntity getGameJoiner(GameUserJoinerEntity gameUserJoinerEntity) throws UserNotFoundException {
        if(gameUserJoinerEntity == null)
            return null;
        return userRepository
                .findById(gameUserJoinerEntity.getUserId().getId())
                .orElseThrow(UserNotFoundException::new);
    }

    public UserEntity getGameJoinerByGameEntity(GameEntity gameEntity) throws UserNotFoundException {
        GameUserJoinerEntity gameUserJoinerEntity = gameUserJoinerRepository.findByGameId(gameEntity);
        if(gameUserJoinerEntity == null)
            return null;
        return userRepository
                .findById(gameUserJoinerEntity.getUserId().getId())
                .orElseThrow(UserNotFoundException::new);
    }

    public UserEntity getGameOwnerByGameEntity(GameEntity gameEntity) throws UserNotFoundException {
        GameUserOwnerEntity gameUserOwnerEntity = gameUserOwnerRepository.findByGameId(gameEntity);
        return userRepository
                .findById(gameUserOwnerEntity.getUserId().getId())
                .orElseThrow(UserNotFoundException::new);
    }

    public UserEntity updateGameOwner(GameEntity gameEntity, Move move, Game game) throws UserNotFoundException {
        GameUserOwnerEntity gameUserOwnerEntity = gameUserOwnerRepository.findByGameId(gameEntity);
        UserEntity gameOwner = getGameOwner(gameUserOwnerEntity);
        updateGameUserOwnerEntity(
                gameUserOwnerEntity.getId(),
                move,
                game,
                gameEntity,
                gameOwner);
        return gameOwner;
    }

}
