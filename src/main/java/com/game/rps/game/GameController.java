package com.game.rps.game;

import com.game.rps.token.TokenService;
import com.game.rps.user.UserNotFoundException;
import com.game.rps.user.UserService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;
import java.util.List;


@RestController
@RequestMapping("/games")
@AllArgsConstructor
public class GameController {

    GameService gameService;
    TokenService tokenService;
    UserService userService;


    @GetMapping("/start")
    public GameStatus startGame(@RequestHeader(value = "token") String tokenId) throws UserNotFoundException, ActiveGameAlreadyExistException {
        return  gameService.createGame(
                    userService.getUser(
                        tokenService.getTokenById(tokenId)
                    )
                );
    }
    @GetMapping("/join/{gameId}")
    public GameStatus joinGame(@PathVariable("gameId") String gameId, @RequestHeader(value = "token") String tokenId) throws UserNotFoundException, ActiveGameAlreadyExistException {
        return  gameService.joinGame(
                    gameId,
                    userService.getUser(
                        tokenService.getTokenById(tokenId)
                    )
        );
    }
    @GetMapping("/status")
    public GameStatus gameStatus(@RequestHeader(value = "token") String tokenId) throws UserNotFoundException, GameNotFoundException {
        return gameService.getGameStatus(
                    userService.getUser(
                        tokenService.getTokenById(tokenId)
                    )
        );
    }
    @GetMapping()
    public List<GameStatus> gameList(@RequestHeader(value = "token") String tokenId) throws UserNotFoundException {
        userService.checkIfUserExist(tokenService.getTokenById(tokenId));
        return gameService.getOpenGameList();
    }
    @GetMapping("/{id}")
    public GameStatus gameInfo(@PathVariable("id") String gameId, @RequestHeader(value = "token") String tokenId) throws UserNotFoundException {
        userService.checkIfUserExist(tokenService.getTokenById(tokenId));
        return gameService.getGameInfo(gameId);
    }

    @GetMapping("/move/{sign}")
    public GameStatus makeMove(@PathVariable String sign, @RequestHeader(value = "token") String tokenId) throws UserNotFoundException, GameNotFoundException {
       return gameService.updateMove(
               sign,
               userService.getUser(
                       tokenService.getTokenById(tokenId)
               )
       );
    }
}
