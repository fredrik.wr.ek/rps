package com.game.rps.user;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Value;


@Value
public class UserName {
    String name;

    @JsonCreator
    public UserName(@JsonProperty("name") String name) { this.name = name; }
}