package com.game.rps.user;

import com.game.rps.token.Token;
import com.game.rps.token.TokenService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/user")
@AllArgsConstructor
public class UserController {
    UserService userService;
    TokenService tokenService;

    @PostMapping("/name")
    public String setName(@RequestBody UserName userName, @RequestHeader(value = "token") String tokenId) throws UserNotFoundException {
        Token token = tokenService.getTokenById(tokenId);
        tokenService.updateToken(userService.setUserName(userName, token));
        return "OK";
    }
}
