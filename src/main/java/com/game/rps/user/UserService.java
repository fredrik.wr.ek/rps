package com.game.rps.user;

import com.game.rps.token.Token;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Service
public class UserService {

    private final UserRepository userRepository;

    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public Token setUserName(UserName userName, Token token) throws UserNotFoundException {
        if(token.getUserId() == null)
            return createNewUser(userName.getName(), token);
        else
            return updateUser(userName.getName(), token);
    }

    public Token createNewUser(String userName, Token token) {
        UserEntity newUser = new UserEntity(UUID.randomUUID().toString(), userName);
        userRepository.save(newUser);
        token.setUserId(newUser.getId());
        return token;
    }
    public Token updateUser(String userName, Token token) throws UserNotFoundException {
        UserEntity updatedUser = userRepository
                .findById(token.getUserId())
                .orElseThrow(UserNotFoundException::new);
        updatedUser.setName(userName);
        userRepository.save(updatedUser);
        return token;
    }
    public UserEntity getUser(Token token) throws UserNotFoundException {
        return userRepository
                .findById(token.getUserId())
                .orElseThrow(UserNotFoundException::new);
    }
    public void checkIfUserExist(Token token) throws UserNotFoundException {
        getUser(token);
    }
}
