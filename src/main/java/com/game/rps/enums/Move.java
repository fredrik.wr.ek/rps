package com.game.rps.enums;

public enum Move {
    ROCK,
    PAPER,
    SCISSOR,
    NONE
}
