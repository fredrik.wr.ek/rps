package com.game.rps.enums;

public enum Game {
    NONE,
    OPEN,
    ACTIVE,
    WIN,
    LOSE,
    DRAW
}
